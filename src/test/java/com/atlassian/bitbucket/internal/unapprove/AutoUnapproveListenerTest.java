package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.concurrent.DelegatingExecutorService;
import com.atlassian.bitbucket.event.pull.PullRequestReopenedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestUpdatedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryDeletedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scopes;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ExecutorService;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AutoUnapproveListenerTest {

    @Mock
    private RescopeAnalyzer analyzer;
    @Spy
    private ExecutorService executorService = new DelegatingExecutorService(MoreExecutors.newDirectExecutorService());
    @InjectMocks
    private AutoUnapproveListener listener;
    @Mock
    private PullRequest pullRequest;
    @Mock
    private InternalAutoUnapproveService unapproveService;

    @Test
    public void testOnPullRequestReopened() {
        when(unapproveService.isEnabled(same(pullRequest))).thenReturn(true);

        listener.onPullRequestReopened(new PullRequestReopenedEvent(this, pullRequest, "previousFrom", "previousTo"));

        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService).withdrawApprovals(same(pullRequest));
    }

    @Test
    public void testOnPullRequestReopenedWhenDisabled() {
        listener.onPullRequestReopened(new PullRequestReopenedEvent(this, pullRequest, "previousFrom", "previousTo"));

        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService, never()).withdrawApprovals(any());
    }

    @Test
    public void testOnPullRequestRescoped() {
        when(analyzer.isUpdated(same(pullRequest), eq("previousFrom"))).thenReturn(true);
        when(pullRequest.isOpen()).thenReturn(true);
        when(unapproveService.isEnabled(same(pullRequest))).thenReturn(true);

        triggerRescopedEvent();

        verify(analyzer).isUpdated(same(pullRequest), eq("previousFrom"));
        verify(executorService).execute(any(Runnable.class));
        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService).withdrawApprovals(same(pullRequest));
    }

    @Test
    public void testOnPullRequestRescopedWhenClosed() {
        triggerRescopedEvent();

        verify(pullRequest).isOpen();
        verifyZeroInteractions(analyzer, executorService, unapproveService);
    }

    @Test
    public void testOnPullRequestRescopedWhenDisabled() {
        when(pullRequest.isOpen()).thenReturn(true);

        triggerRescopedEvent();

        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService, never()).withdrawApprovals(any());
        verifyZeroInteractions(analyzer, executorService);
    }

    @Test
    public void testOnPullRequestRescopedWhenUnchanged() {
        when(pullRequest.isOpen()).thenReturn(true);
        when(unapproveService.isEnabled(same(pullRequest))).thenReturn(true);

        triggerRescopedEvent();

        verify(analyzer).isUpdated(same(pullRequest), eq("previousFrom"));
        verify(executorService).execute(any(Runnable.class));
        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService, never()).withdrawApprovals(any());
    }

    @Test
    public void testOnPullRequestUpdated() {
        PullRequestRef currentTo = mockPullRequestRef("currentCommit");
        Ref previousTo = mockRef("previousCommit");

        when(pullRequest.getToRef()).thenReturn(currentTo);
        when(pullRequest.isOpen()).thenReturn(true);
        when(unapproveService.isEnabled(same(pullRequest))).thenReturn(true);

        listener.onPullRequestUpdated(new PullRequestUpdatedEvent(this, pullRequest, "Title", null, previousTo));

        verify(currentTo).getLatestCommit();
        verify(previousTo).getLatestCommit();
        verify(pullRequest).getToRef();
        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService).withdrawApprovals(same(pullRequest));
    }

    @Test
    public void testOnPullRequestUpdatedWhenClosed() {
        listener.onPullRequestUpdated(new PullRequestUpdatedEvent(this, pullRequest, "Title", null, mock(Ref.class)));

        verify(pullRequest).isOpen();
        verifyZeroInteractions(unapproveService);
    }

    @Test
    public void testOnPullRequestUpdatedWhenDisabled() {
        when(pullRequest.isOpen()).thenReturn(true);

        listener.onPullRequestUpdated(new PullRequestUpdatedEvent(this, pullRequest, "Title", null, mock(Ref.class)));

        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService, never()).withdrawApprovals(any());
    }

    @Test
    public void testOnPullRequestUpdatedWhenRetargetedToEquivalentBranch() {
        PullRequestRef currentTo = mockPullRequestRef("commit");
        Ref previousTo = mockRef("commit");

        when(pullRequest.getToRef()).thenReturn(currentTo);
        when(pullRequest.isOpen()).thenReturn(true);
        when(unapproveService.isEnabled(same(pullRequest))).thenReturn(true);

        listener.onPullRequestUpdated(new PullRequestUpdatedEvent(this, pullRequest, "Title", null, previousTo));

        verify(currentTo).getLatestCommit();
        verify(previousTo).getLatestCommit();
        verify(pullRequest).isOpen();
        verify(unapproveService).isEnabled(same(pullRequest));
        verify(unapproveService, never()).withdrawApprovals(any());
    }

    @Test
    public void testOnPullRequestUpdatedWithoutRetargeting() {
        listener.onPullRequestUpdated(new PullRequestUpdatedEvent(this, pullRequest, "Title", null, null));

        verifyZeroInteractions(unapproveService);
    }

    @Test
    public void testOnRepositoryDeleted() {
        Repository repository = mock(Repository.class);

        listener.onRepositoryDeleted(new RepositoryDeletedEvent(this, repository, emptyList()));

        verify(unapproveService).removeSettings(eq(Scopes.repository(repository)));
    }

    private static PullRequestRef mockPullRequestRef(String commit) {
        PullRequestRef currentTo = mock(PullRequestRef.class);
        when(currentTo.getLatestCommit()).thenReturn(commit);

        return currentTo;
    }

    private static Ref mockRef(String previousCommit) {
        Ref previousTo = mock(Ref.class);
        when(previousTo.getLatestCommit()).thenReturn(previousCommit);

        return previousTo;
    }

    private void triggerRescopedEvent() {
        PullRequestRef fromRef = mockPullRequestRef("currentFrom");
        PullRequestRef toRef = mockPullRequestRef("currentTo");

        when(pullRequest.getFromRef()).thenReturn(fromRef);
        when(pullRequest.getToRef()).thenReturn(toRef);

        listener.onPullRequestRescoped(new PullRequestRescopedEvent(this, pullRequest, "previousFrom", "previousTo"));
    }
}
