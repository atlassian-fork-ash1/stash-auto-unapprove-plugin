package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.i18n.SimpleI18nService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.*;
import com.atlassian.bitbucket.transaction.SimpleSalTransactionTemplate;
import com.atlassian.bitbucket.unapprove.AutoUnapproveGlobalSettingsChangedEvent;
import com.atlassian.bitbucket.unapprove.AutoUnapproveProjectSettingsChangedEvent;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettingsChangedEvent;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.DummySecurityService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.validation.ArgumentValidationException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static com.atlassian.bitbucket.internal.unapprove.DefaultAutoUnapproveService.PLUGIN_KEY;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoUnapproveServiceTest {

    @Mock
    private EventPublisher eventPublisher;
    private GlobalScope globalScope = Scopes.global();
    @Spy
    private I18nService i18nService = new SimpleI18nService();
    @Mock
    private Project project;
    private ProjectScope projectScope;
    @Mock
    private PullRequestService pullRequestService;
    @Mock
    private Repository repository;
    private RepositoryScope repositoryScope;
    @Spy
    private SecurityService securityService = new DummySecurityService();
    private DefaultAutoUnapproveService service;
    @Mock
    private PluginSettings settings;
    @Mock
    private PluginSettingsFactory settingsFactory;
    @Spy
    private TransactionTemplate transactionTemplate = new SimpleSalTransactionTemplate();
    @Mock
    private PermissionValidationService validationService;

    @Before
    public void setup() {
        when(repository.getId()).thenReturn(42);
        when(repository.getProject()).thenReturn(project);
        when(project.getId()).thenReturn(99);
        when(settingsFactory.createSettingsForKey(eq(PLUGIN_KEY))).thenReturn(settings);

        repositoryScope = Scopes.repository(repository);
        projectScope = Scopes.project(project);

        service = new DefaultAutoUnapproveService(eventPublisher, i18nService, settingsFactory, pullRequestService,
                securityService, transactionTemplate, validationService);
    }

    @Test
    public void testGetSettingsForGlobal() {
        // Initial state
        assertSettings(service.getSettings(globalScope), globalScope, false);

        // Enable
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("1");
        assertSettings(service.getSettings(globalScope), globalScope, true);

        // Disable
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("0");
        assertSettings(service.getSettings(globalScope), globalScope, false);
    }

    @Test
    public void testGetSettingsForProjectCustomSettings() {
        // Initial state
        assertSettings(service.getSettings(projectScope), globalScope, false);

        // Custom setting (disabled)
        when(settings.get(eq("project.99.auto.unapprove"))).thenReturn("0");
        assertSettings(service.getSettings(projectScope), projectScope, false);

        // Custom setting (enabled)
        when(settings.get(eq("project.99.auto.unapprove"))).thenReturn("1");
        assertSettings(service.getSettings(projectScope), projectScope, true);
    }

    @Test
    public void testGetSettingsForProjectInheritGlobal() {
        // Initial state
        assertSettings(service.getSettings(projectScope), globalScope, false);

        // Inheriting global setting (disabled)
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("0");
        assertSettings(service.getSettings(projectScope));

        // Inheriting global settings (enabled)
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("1");
        assertSettings(service.getSettings(projectScope), globalScope, true);
    }

    @Test
    public void testGetSettingsForPullRequestGlobalScope() {
        PullRequest pullRequest = mockPullRequest();
        assertFalse(service.isEnabled(pullRequest));
        assertSettings(service.getSettings(pullRequest), globalScope, false);

        when(settings.get(eq("global.auto.unapprove"))).thenReturn("1");

        assertTrue(service.isEnabled(pullRequest));
        AutoUnapproveSettings settings = service.getSettings(pullRequest);
        assertSettings(settings, globalScope, true);
    }

    @Test
    public void testGetSettingsForPullRequestProjectScope() {
        PullRequest pullRequest = mockPullRequest();
        assertFalse(service.isEnabled(pullRequest));
        assertSettings(service.getSettings(pullRequest), globalScope, false);

        when(settings.get(eq("project.99.auto.unapprove"))).thenReturn("1");

        assertTrue(service.isEnabled(pullRequest));
        AutoUnapproveSettings settings = service.getSettings(pullRequest);
        assertSettings(settings, projectScope, true);
    }

    @Test
    public void testGetSettingsForPullRequestRepositoryScope() {
        PullRequest pullRequest = mockPullRequest();
        assertFalse(service.isEnabled(pullRequest));
        assertSettings(service.getSettings(pullRequest), globalScope, false);

        when(settings.get(eq("repo.42.auto.unapprove"))).thenReturn("1");

        assertTrue(service.isEnabled(pullRequest));
        AutoUnapproveSettings settings = service.getSettings(pullRequest);
        assertSettings(settings, repositoryScope, true);
    }

    @Test
    public void testGetSettingsForRepositoryCustomSettings() {
        // Initial state
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), globalScope, false);

        // Custom setting (disabled)
        when(settings.get(eq("repo.42.auto.unapprove"))).thenReturn("0");
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), repositoryScope, false);

        // Custom setting (enabled)
        when(settings.get(eq("repo.42.auto.unapprove"))).thenReturn("1");
        assertTrue(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), repositoryScope, true);
    }

    @Test
    public void testGetSettingsForRepositoryInheritGlobal() {
        // Initial state
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), globalScope, false);

        // Inheriting global setting (disabled)
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("0");
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), globalScope, false);

        // Inheriting global settings (enabled)
        when(settings.get(eq("global.auto.unapprove"))).thenReturn("1");
        assertTrue(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), globalScope, true);
    }

    @Test
    public void testGetSettingsForRepositoryInheritProject() {
        // Initial state
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), globalScope, false);

        // Inheriting project setting (disabled)
        when(settings.get(eq("project.99.auto.unapprove"))).thenReturn("0");
        assertFalse(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), projectScope, false);

        // Inheriting project setting (enabled)
        when(settings.get(eq("project.99.auto.unapprove"))).thenReturn("1");
        assertTrue(service.isEnabled(repository));
        assertSettings(service.getSettings(repositoryScope), projectScope, true);
    }

    @Test(expected = ArgumentValidationException.class)
    public void testRemoveSettingsForGlobal() {
        service.removeSettings(globalScope);
    }

    @Test
    public void testRemoveSettingsForProject() {
        service.removeSettings(projectScope);

        verify(settings).remove(eq("project.99.auto.unapprove"));
        verify(validationService).validateForProject(same(project), same(Permission.PROJECT_ADMIN));
    }

    @Test(expected = AuthorisationException.class)
    public void testRemoveSettingsForProjectNoPermission() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForProject(same(project), same(Permission.PROJECT_ADMIN));
        service.removeSettings(projectScope);
    }

    @Test
    public void testRemoveSettingsForRepository() {
        service.removeSettings(repositoryScope);

        verify(settings).remove(eq("repo.42.auto.unapprove"));
        verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));
    }

    @Test(expected = AuthorisationException.class)
    public void testRemoveSettingsForRepositoryNoPermission() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForRepository(same(repository), same(Permission.REPO_ADMIN));
        service.removeSettings(repositoryScope);
    }

    @Test
    public void testSetSettingsForGlobalDisabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(globalScope)
                .enabled(false)
                .build());

        ArgumentCaptor<AutoUnapproveGlobalSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveGlobalSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("global.auto.unapprove"), eq("0"));
        verify(validationService).validateForGlobal(same(Permission.ADMIN));

        AutoUnapproveGlobalSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(service, event.getSource());
        assertFalse(event.isEnabled());
    }

    @Test
    public void testSetSettingsForGlobalEnabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(globalScope)
                .enabled(true)
                .build());

        ArgumentCaptor<AutoUnapproveGlobalSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveGlobalSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("global.auto.unapprove"), eq("1"));
        verify(validationService).validateForGlobal(same(Permission.ADMIN));

        AutoUnapproveGlobalSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(service, event.getSource());
        assertTrue(event.isEnabled());
    }

    @Test
    public void testSetSettingsForProjectDisabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(projectScope)
                .enabled(false)
                .build());

        ArgumentCaptor<AutoUnapproveProjectSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveProjectSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("project.99.auto.unapprove"), eq("0"));
        verify(validationService).validateForProject(same(project), same(Permission.PROJECT_ADMIN));

        AutoUnapproveProjectSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(project, event.getProject());
        assertSame(service, event.getSource());
        assertFalse(event.isEnabled());
    }

    @Test
    public void testSetSettingsForProjectEnabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(projectScope)
                .enabled(true)
                .build());

        ArgumentCaptor<AutoUnapproveProjectSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveProjectSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("project.99.auto.unapprove"), eq("1"));
        verify(validationService).validateForProject(same(project), same(Permission.PROJECT_ADMIN));

        AutoUnapproveProjectSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(project, event.getProject());
        assertSame(service, event.getSource());
        assertTrue(event.isEnabled());
    }

    @Test
    public void testSetSettingsForRepositoryDisabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(repositoryScope)
                .enabled(false)
                .build());

        ArgumentCaptor<AutoUnapproveSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("repo.42.auto.unapprove"), eq("0"));
        verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        AutoUnapproveSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(repository, event.getRepository());
        assertSame(service, event.getSource());
        assertFalse(event.isEnabled());
    }

    @Test
    public void testSetSettingsForRepositoryEnabled() {
        service.setSettings(SimpleAutoUnapproveSettings.builder(repositoryScope)
                .enabled(true)
                .build());

        ArgumentCaptor<AutoUnapproveSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq("repo.42.auto.unapprove"), eq("1"));
        verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        AutoUnapproveSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(repository, event.getRepository());
        assertSame(service, event.getSource());
        assertTrue(event.isEnabled());
    }

    @Test(expected = AuthorisationException.class)
    public void testSetSettingsWithoutPermissionForGlobal() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForGlobal(same(Permission.ADMIN));

        try {
            service.setSettings(SimpleAutoUnapproveSettings.builder(globalScope)
                    .enabled(true)
                    .build());
        } finally {
            verify(validationService).validateForGlobal(same(Permission.ADMIN));
            verifyZeroInteractions(eventPublisher, settings);
        }
    }

    @Test(expected = AuthorisationException.class)
    public void testSetSettingsWithoutPermissionForProject() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForProject(same(project), same(Permission.PROJECT_ADMIN));

        try {
            service.setSettings(SimpleAutoUnapproveSettings.builder(Scopes.project(project))
                    .enabled(true)
                    .build());
        } finally {
            verify(validationService).validateForProject(same(project), same(Permission.PROJECT_ADMIN));
            verifyZeroInteractions(eventPublisher, settings);
        }
    }

    @Test(expected = AuthorisationException.class)
    public void testSetSettingsWithoutPermissionForRepository() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        try {
            service.setSettings(SimpleAutoUnapproveSettings.builder(repositoryScope)
                    .enabled(true)
                    .build());
        } finally {
            verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));
            verifyZeroInteractions(eventPublisher, settings);
        }
    }

    @Test
    public void testWithdrawApprovals() {
        PullRequestParticipant first = mockParticipant();
        PullRequestParticipant second = mockParticipant();

        PullRequest pullRequest = mockPullRequest();
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(first, second));

        service.withdrawApprovals(pullRequest);

        InOrder ordered = inOrder(pullRequestService, securityService, transactionTemplate);
        ordered.verify(transactionTemplate).execute(any());
        ordered.verify(securityService).impersonating(same(first.getUser()), notNull());
        ordered.verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));
        ordered.verify(securityService).impersonating(same(second.getUser()), notNull());
        ordered.verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));

        verify(pullRequest, never()).getParticipants(); // Participants should not be reset; they can't approve
        verify(pullRequest).getReviewers();             // Reviewers should always be reset
    }

    @Test
    public void testWithdrawApprovalsHandlesIllegalPullRequestState() {
        doThrow(IllegalPullRequestStateException.class)
                .when(pullRequestService)
                .setReviewerStatus(anyInt(), anyLong(), any());

        PullRequestParticipant participant = mockParticipant();

        PullRequest pullRequest = mockPullRequest();
        when(pullRequest.getReviewers()).thenReturn(Collections.singleton(participant));

        service.withdrawApprovals(pullRequest);

        verify(pullRequest, never()).getParticipants();
        verify(pullRequest).getReviewers();
        verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));
    }

    private static PullRequestParticipant mockParticipant() {
        ApplicationUser user = mock(ApplicationUser.class);

        PullRequestParticipant participant = mock(PullRequestParticipant.class);
        when(participant.getUser()).thenReturn(user);

        return participant;
    }

    private void assertSettings(AutoUnapproveSettings initialSettings, Scope scope, boolean enabled) {
        assertThat(initialSettings.getScope(), equalTo(scope));
        assertThat(initialSettings.isEnabled(), equalTo(enabled));
    }

    private void assertSettings(AutoUnapproveSettings updatedSettings) {
        assertSettings(updatedSettings, globalScope, false);
    }

    private PullRequest mockPullRequest() {
        PullRequestRef toRef = mock(PullRequestRef.class);
        when(toRef.getRepository()).thenReturn(repository);

        PullRequest pullRequest = mock(PullRequest.class);
        when(pullRequest.getId()).thenReturn(1L);
        when(pullRequest.getToRef()).thenReturn(toRef);

        return pullRequest;
    }
}
