package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.server.StorageService;
import com.atlassian.bitbucket.util.MoreFiles;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * @since 2.2
 */
public class DefaultAutoUnapproveConfig implements AutoUnapproveConfig {

    private final File tempDir;

    public DefaultAutoUnapproveConfig(StorageService storageService) {
        tempDir = MoreFiles.mkdir(storageService.getTempDir(), "unapprove").toFile();
    }

    @Nonnull
    @Override
    public File getTempDir() {
        return tempDir;
    }
}
