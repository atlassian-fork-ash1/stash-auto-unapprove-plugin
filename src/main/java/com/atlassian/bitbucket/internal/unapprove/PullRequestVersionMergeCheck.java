package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.hook.repository.PreRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.PullRequestMergeHookRequest;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeCheck;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestParticipant;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

public class PullRequestVersionMergeCheck implements RepositoryMergeCheck {

    private final RescopeAnalyzer rescopeAnalyzer;
    private final InternalAutoUnapproveService unapproveService;

    public PullRequestVersionMergeCheck(InternalAutoUnapproveService unapproveService, RescopeAnalyzer rescopeAnalyzer) {
        this.unapproveService = unapproveService;
        this.rescopeAnalyzer = rescopeAnalyzer;
    }

    @Nonnull
    @Override
    public RepositoryHookResult preUpdate(@Nonnull PreRepositoryHookContext context,
                                          @Nonnull PullRequestMergeHookRequest request) {
        PullRequest pullRequest = request.getPullRequest();
        if (request.isDryRun() || !unapproveService.isEnabled(pullRequest)) {
            return RepositoryHookResult.accepted();
        }

        String latestCommit = request.getFromRef().getLatestCommit();
        RepositoryHookResult.Builder resultBuilder = new RepositoryHookResult.Builder();
        Predicate<String> memorizingAnalyzer = new Predicate<String>() {

            private final Map<String, Boolean> cache = new HashMap<>();

            @Override
            public boolean test(String commitHash) {
                return cache.computeIfAbsent(commitHash, hash -> rescopeAnalyzer.isUpdated(pullRequest, hash));
            }
        };
        pullRequest.getReviewers().stream()
                .filter(PullRequestParticipant::isApproved)
                .forEach(approver -> {
                    String latestReviewedCommit = approver.getLastReviewedCommit();
                    if (latestReviewedCommit != null && // The reviewer has reviewed a commit
                            !Objects.equals(latestCommit, latestReviewedCommit) &&  // The reviewed commit is different to what the PR is at now
                            memorizingAnalyzer.test(latestReviewedCommit)) { // The different commit also provides a different diff
                        String username = approver.getUser().getName();
                        resultBuilder.veto("User " + username + " has not reviewed the latest changes",
                                "Pull request is currently at " + latestCommit + " but " + username +
                                        "'s latest reviewed commit is " + latestReviewedCommit);
                    }
                });
        RepositoryHookResult result = resultBuilder.build();

        if (result.isRejected()) {
            unapproveService.withdrawApprovals(pullRequest);
        }

        return result;
    }
}
