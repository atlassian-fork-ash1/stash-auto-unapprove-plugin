package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapper;
import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class AutoUnapproveExceptionMapper extends UnhandledExceptionMapper {

    public AutoUnapproveExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }
}
