package com.atlassian.bitbucket.internal.unapprove;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * @since 2.2
 */
public interface AutoUnapproveConfig {

    /**
     * Retrieves the directory where the plugin should store any temporary files it creates.
     *
     * @return the directory for temporary files
     */
    @Nonnull
    File getTempDir();
}
