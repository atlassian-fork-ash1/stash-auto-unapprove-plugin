package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.Channels;
import com.atlassian.bitbucket.audit.Priority;
import com.atlassian.bitbucket.event.annotation.Audited;
import com.atlassian.bitbucket.event.project.ProjectEvent;
import com.atlassian.bitbucket.project.Project;

import javax.annotation.Nonnull;

/**
 * @since 4.1
 */
@Audited(converter = AutoUnapproveProjectSettingsChangedEventConverter.class,
         channels = {Channels.PROJECT_UI}, priority = Priority.HIGH)
public class AutoUnapproveProjectSettingsChangedEvent extends ProjectEvent {

    private final boolean enabled;

    public AutoUnapproveProjectSettingsChangedEvent(@Nonnull Object source, @Nonnull Project project, boolean enabled) {
        super(source, project);
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
