package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.Priority;
import com.atlassian.bitbucket.event.ApplicationEvent;
import com.atlassian.bitbucket.event.annotation.Audited;

import javax.annotation.Nonnull;

/**
 * @since 4.1
 */
@Audited(converter = AutoUnapproveGlobalSettingsChangedEventConverter.class, priority = Priority.HIGH)
public class AutoUnapproveGlobalSettingsChangedEvent extends ApplicationEvent {

    private final boolean enabled;

    public AutoUnapproveGlobalSettingsChangedEvent(@Nonnull Object source, boolean enabled) {
        super(source);
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
